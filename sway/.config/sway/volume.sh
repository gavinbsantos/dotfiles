#!/bin/bash

vol=$(wpctl get-volume @DEFAULT_AUDIO_SINK@)

case $1 in
  "up")
    # Clamp at 1
    # if [[ $( printf "%.0f" $vol ) -ge 1 ]]; then
    #   wpctl set-volume @DEFAULT_AUDIO_SINK@ 1
    # else
    #   wpctl set-volume @DEFAULT_AUDIO_SINK@ $2%+
    # fi
    wpctl set-volume @DEFAULT_AUDIO_SINK@ $2%+
    ;;
  "down")
    # already clamped at 0
    wpctl set-volume @DEFAULT_AUDIO_SINK@ $2%-
    ;;
  *)
    wpctl get-volume @DEFAULT_AUDIO_SINK@
esac

