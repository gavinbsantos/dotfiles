#!/bin/bash
dt=$(date +"%a %Y-%m-%d %H:%M:%S")
battery_status=$(cat /sys/class/power_supply/BAT1/capacity)
get_mem() {
  total=$(awk '/^MemTotal: / {print $2}' /proc/meminfo)
  free=$(awk '/^MemAvailable: / {print $2}' /proc/meminfo)
  used=$((total - free))
  echo -n "Mem: $((used*100/total))%"
}

get_vol() {
  echo -n "Vol: $(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{for (i=2; i<=NF; i++) print $i}' | tr -d \[:punct:\])%"
  # echo -n "Vol2: $(./volume.sh status)%"
}

get_cmus() {
  output=$(cmus-remote -Q)
  # message=""
  path=$(echo "$output" | awk '/^file/ {print}' | cut -c 12-)
  song=$(echo "$output" | awk '/^tag title/ {print}' | cut -c 11-)
  artist=$(echo "$output" | awk '/^tag artist/ {print}' | cut -c 12-)
  cmusstatus=$(echo "$output" | awk '/^status/ {print $NF}')
  duration=$(echo "$output" | awk '/^duration/ {print $NF}')
  position=$(echo "$output" | awk '/^position/ {print $NF}')

  # echo -n "^ca(1, cmus-remote -u)"
  case $cmusstatus in
    "playing")
      echo -n " ►"
      # echo -n "^i($bitmaps/play.xbm)"
      ;;
    "paused")
      echo -n "▐▐"
      # echo -n "^i($bitmaps/pause.xbm)"
      ;;
    "stopped")
      echo -n ""
      # This might cause a problem
      return 0
      ;;
    *)
      return 0
      ;;
  esac

  echo -n " ($(expr $duration - $position | xargs -I u date -d@u +-%M:%S)"
  echo -n "/"
  echo -n "$(date -d@$duration +%M:%S)) "

  if [[ $artist = *[!\ ]* ]]; then
    echo -n "$artist - $song |"
  elif [[ $path = *[!\ ]* ]]; then
    echo $path | awk -F "/" '{print $NF} |' | xargs echo -n
  fi

  # echo -n "^ca()"
}

get_keys() {
  echo $(swaymsg -t get_inputs | jq 'map(select(has("xkb_active_layout_name")))[0].xkb_active_layout_name')
}

echo $(get_cmus) $(get_keys) \| $(get_vol) \| $(get_mem) \| Batt: $battery_status% \| $dt
