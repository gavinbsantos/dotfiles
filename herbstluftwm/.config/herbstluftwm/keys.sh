#! /usr/bin/bash

if [ $(setxkbmap -query | awk '/variant:/ {print $2}') = 'dvorak' ]; then
  setxkbmap -layout us 
  echo us
else
  setxkbmap -layout us -variant dvorak
  echo dv
fi
