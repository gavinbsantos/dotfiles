set fish_greeting

abbr --add nv nvim
abbr --add nv. nvim .

abbr --add tn tmux new -s
abbr --add ta tmux a

abbr --add za zathura

abbr --add gp git pull
abbr --add gl git log
abbr --add glo git log --oneline
abbr --add gloga git log --oneline --graph --all

abbr --add start ~/.start.sh

abbr --add ll ls -hal

bind \cG forward-word
bind \cF accept-autosuggestion

# Setup a starship prompt
starship init fish | source

if status is-interactive
    # Commands to run in interactive sessions can go here
end
