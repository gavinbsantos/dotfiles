(custom-set-variables
 '(auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaves\\1" t)))
 '(backup-directory-alist '((".*" . "~/.emacs.d/backups")))
 '(undo-tree-history-directory-alist '((".*" . "~/.emacs.d/undo-tree"))))

(defvar thingy/start-page-file "~/various_stuff/start-page.org")
(message "thingy")
(setq initial-buffer-choice thingy/start-page-file)
