;; --- Custom hell for custom-set-variables ---
(setq custom-file "./custom.el")
(load (expand-file-name "custom.el" user-emacs-directory))
;; --- End of Custom hell for custom-set-variables ---

(pcase system-type
  ('windows-nt (load (expand-file-name "windows.el" user-emacs-directory)))
  ('gnu/linux (load (expand-file-name "linux.el" user-emacs-directory))))
;; --- Miscellaneous Variables ---
(setq inhibit-startup-message nil)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(tab-bar-mode t)
(show-paren-mode t)
(display-time-mode -1)
(setq display-time-24hr-format 1)
(setq show-paran-style 'expression)
(setq-default indent-tabs-mode nil)
(setq display-line-numbers-mode t)
(setq-default display-line-numbers 'relative)
(setq visible-bell 1)
(set-language-environment "UTF-8")
(setq initial-major-mode 'org-mode)
      
(setq c-default-style "bsd")
(setq c-basic-offset 2)
(setq-default tab-width 2)
(setq indent-line-function 'insert-tab)
(setq backwards-delete-char-untabify-method 'hungry)

(setq column-number-mode t)
(setq-default fill-column 79)

(setq flyspell-large-region 1)

(put 'dired-find-alternate-file 'disabled nil)

;; Code from https://github.com/ramnes/move-border/ (I too am not the original author)
(defun xor (b1 b2)
  (or (and b1 b2)
      (and (not b1) (not b2))))

(defun move-border-left-or-right (arg dir)
  "General function covering move-border-left and move-border-right. If DIR is
     t, then move left, otherwise move right."
  (interactive)
  (if (null arg) (setq arg 3))
  (let ((left-edge (nth 0 (window-edges))))
    (if (xor (= left-edge 0) dir)
        (shrink-window arg t)
      (enlarge-window arg t))))

(defun move-border-up-or-down (arg dir)
  "General function covering move-border-up and move-border-down. If DIR is
     t, then move up, otherwise move down."
  (interactive)
  (if (null arg) (setq arg 3))
  (let ((top-edge (nth 1 (window-edges))))
    (if (xor (= top-edge 0) dir)
        (shrink-window arg nil)
      (enlarge-window arg nil))))

(defun move-border-left (arg)
  (interactive "P")
  (move-border-left-or-right arg t))

(defun move-border-right (arg)
  (interactive "P")
  (move-border-left-or-right arg nil))

(defun move-border-up (arg)
  (interactive "P")
  (move-border-up-or-down arg t))

(defun move-border-down (arg)
  (interactive "P")
  (move-border-up-or-down arg nil))
;; End of stolen code


;; --- Preamble Packages ---
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; --- Editing Stuff ---
(add-hook 'text-mode-hook 'turn-on-auto-fill)
;; (add-hook 'shell-mode-hook
;;           (setq comint-process-echoes t))
;; (rainbow-mode)

;; (use-package disable-mouse
;;   :config
;;   (global-disable-mouse-mode))

(use-package undo-tree
             :config
             (global-undo-tree-mode))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-undo-system 'undo-tree)
  (setq evil-shift-width tab-width)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (evil-mode 1)
  :config
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line))

(use-package evil-surround
  :requires evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-commentary
  :requires evil
  :config (evil-commentary-mode))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package gdscript-mode)

(use-package auctex
  :defer t)

(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(use-package pdf-tools
  :init
  (pdf-tools-install))

(add-hook 'pdf-view-mode-hook
          (blink-cursor-mode -1))

(use-package darkroom
  :config
  (setq darkroom-text-scale-increase 1))

(use-package rainbow-mode
  :init
  (rainbow-mode))

;; (use-package selectric-mode)

(use-package nyan-mode
  :config
  (nyan-mode)
  (nyan-start-animation)
  (setq nyan-wavy-trail 1))

;; --- helpful packages ---
(use-package which-key
  :init
  (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.5))

(use-package helpful
  :custom
  (counsel-describe-function #'helpful-callable)
  (counsel-describe-variable #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-describe-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

;; (use-package openwith
;;   :config
;;   (openwith-mode t))

(use-package ivy
  :diminish
  :bind (:map ivy-minibuffer-map
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package counsel
  :bind (("M-x" . 'counsel-M-x)
         ("C-x b" . 'counsel-ibuffer)
         ("C-x C-f" . 'counsel-find-file)
         ("C-x b" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil)) ;; Don't start searches with "^"

(use-package ivy-rich
  :requires counsel
  :init
  (ivy-rich-mode t))

(use-package swiper)

(use-package beacon
  :config
  (beacon-mode 1))

(use-package org)


(use-package yasnippet
  :config
  (yas-global-mode t))

(use-package yasnippet-snippets)



;; --- LSP and programming stuff ---
(defun thingy/toggle-line-numbers ()
  (interactive)
    (cond ((eq display-line-numbers 'relative)
                                (setq display-line-numbers t))
          ((eq display-line-numbers t)
           (setq display-line-numbers 'relative))
          (t (setq display-line-numbers 'relative))))
 
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when '(file-directory-p "~/projects")
        (setq projectile-project-search-path '("~/projects")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c C-l")
  :config
  (setq lsp-enable-which-key-integration t)
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-sideline-enable nil)
  (setq lsp-enable-relative-indentation t)
  (setq lsp-headerline-breadcrumb-enable nil)
  (setq lsp-mode-line-code-actions-enable nil))

(use-package lsp-ivy)

(use-package company
  :init
  (global-company-mode))

(use-package magit)

(use-package ace-jump-mode
  :config
  (setq ace-jump-mode-scope 'visible))

(use-package ace-window)

;; --- Hooks and stuff ---
(add-hook 'c-mode-hook
          'electric-pair-local-mode)

(add-hook 'vue-mode-hook
					'electric-pair-local-mode)

(add-hook 'pdf-view-mode-hook
          'menu-bar--display-line-numbers-mode-none)

;; --- Windows And Macros Stuff ---
(use-package hydra)
(defhydra hydra-window-manage (:foreign-keys warn
                               :hint nil)
"
Border: C-hjkl  Tab: M-  ace-window: SPC  bookmark: m
split window: _?  rename: r  delete: d
buffer: b  text scale: =-  (e)shell: sS  file: f  cols: c  quit: q
"

  ("h" windmove-left)
  ("j" windmove-down)
  ("k" windmove-up)
  ("l" windmove-right)

  ("C-h" move-border-left)
  ("C-j" move-border-down)
  ("C-k" move-border-up)
  ("C-l" move-border-right)

  ("_" split-window-below)
  ("?" split-window-right)
  ("r" rename-buffer)
  ("d" delete-window)
  ("b" switch-to-buffer)

  ("M-l" tab-bar-switch-to-next-tab)
  ("M-h" tab-bar-switch-to-prev-tab)
  ("C-M-l" (tab-bar-move-tab 1))
  ("C-M-h" (tab-bar-move-tab -1))
  ("M-t" tab-bar-new-tab)
  ("M-d" tab-bar-close-tab)
  ("M-r" tab-bar-rename-tab)

  ("=" text-scale-increase)
  ("-" text-scale-decrease)

  ("c" display-fill-column-indicator-mode)
  ("s" eshell)
  ("S" shell)
  ("f" find-file)
  ("m" bookmark-jump)

  ("q" nil :color blue))

;; --- Keybinding stuff ---
(global-unset-key (kbd "C-x C-b"))
(global-unset-key (kbd "C-SPC"))

(use-package general)
(general-define-key
 :states 'normal
 :prefix "SPC"
 :global-prefix "C-SPC"

 "i" '(lambda () (interactive) (find-file "~/.emacs.d/init.el"))
 "C-q C-q" 'kill-emacs
 "qq" 'delete-frame
 ;; --- buffers ---
 "b" '(:ignore b :which-key "Buffers")
 "b?" '(split-window-right :which-key "Split right")
 "b_" '(split-window-below :which-key "Split down")
 "br" '(rename-buffer :which-key "Rename buffer")
 "bf" '(counsel-find-file :which-key "Find file")
 "bb" '(counsel-switch-buffer :which-key "Switch buffer")
 "bi" '(ibuffer :which-key "Ibuffer")
 "bk" '(kill-buffer :which-key "Kill buffer")
 "bw" '(hydra-window-manage/body :which-key "Buffer stuff")
 "bo" '(mode-line-other-buffer :which-key "Other buffer")
 "SPC" '(ace-jump-char-mode (read-char) :which-key "Jump char")
 "C-SPC" 'ace-window

 "h" '(windmove-left :which-key "move left")
 "j" '(windmove-down :which-key "move down")
 "k" '(windmove-up :which-key "move up")
 "l" '(windmove-right :which-key "move right")

 ;; --- looks ---
 "a" '(:ignore a :which-key "Asthetics stuff")
 "at" '(thingy/toggle-theme-mode :which-key "Toggle theme")
 "ad" '(darkroom-mode :which-key "Distraction toggle")
 "ac" '(display-fill-column-indicator-mode :which-key "79 cols")
 "ab" '(beacon-blink :which-key "Blink cursor")
 "as" '(text-scale-adjust :which-key "Text size")
 "an" '(thingy/toggle-line-numbers :which-key "Toggle numbers")
 "aN" '(menu-bar--display-line-numbers-mode-none :which-key "Turn off numbers")
 "af" '(toggle-text-mode-auto-fill :which-key "Toggle Auto-fill")

 ;; --- tabs ---
 "t" '(:ignore t :which-key "Tab stuff")
 "td" '(tab-bar-close-tab :which-key "Close tab")
 "tt" '(tab-bar-new-tab :which-key "New tab")
 "ts" '(tab-bar-switch-to-tab :which-key "Switch tabs")
 "tr" '(tab-bar-rename-tab :which-key "Rename tab")

 ;; --- programs and files ---
 "o" '(:ignore o :which-key "Open stuff")
 "oa" '(calendar :which-key "calendar")
 "os" '(eshell :which-key "Eshell")
 "oS" '(shell :which-key "Shell")
 "oc" '(calc :which-key "Calc")
 "oo" '(find-file thingy/start-page-file :which-key "Start page")
 
 "d" '(delete-window :which-key "Delete window")
 "e" '(eval-buffer :which-key "Eval buffer")
 "m" '(bookmark-jump :which-key "Bookmarks")
 "u" '(counsel-unicode-char :which-key "Insert char")
 "s" '(flyspell-buffer :which-key "Spell check time!")
 "S" '(flyspell-region :which-key "Spell check time! (region)")

 "C-c" '(comment-or-uncomment-region :which-key "(un)comment region")
 )

(general-define-key
 :keymaps 'tetris-mode-map
 ;; "j" 'tetris-rotate-next
 ;; "k" 'tetris-rotate-prev
 ;; "<right>" 'tetris
 ;; "<up>" 'tetris-move-bottom
 ;; "<down>" 'tetris
 ;; "<left>" 'tetris
 )


(use-package dired-quick-sort)

(general-define-key
 :keymaps 'dired-mode-map
 "C-s" 'hydra-dired-quick-sort/body)


;; --- Extras ---
;; Insert EMMS here

;; --- Visual stuff ---
(defcustom thingy/default-font-size 100 "Default font size to use")
(use-package all-the-icons
  :if (display-graphic-p))

;; if doom-modeline has some missing icons then run
;; M-x all-the-icons-install-fonts
;; M-x nerd-icons-install-fonts

(use-package doom-modeline
  :init
  (doom-modeline-mode 1))

(use-package doom-themes)

(defcustom thingy/dark-theme 'doom-gruvbox "Dark theme to use")
(defcustom thingy/light-theme 'doom-solarized-light "Light theme to use")
(defvar thingy/current-theme thingy/dark-theme "Current theme in use")
(load-theme thingy/current-theme t)

(defun thingy/change-theme (theme)
  (progn (disable-theme thingy/current-theme))
  (progn (load-theme theme t))
  (setq thingy/current-theme theme))

(defun thingy/toggle-theme-mode ()
  "Toggles between light and dark mode"
  (interactive)
  (cond ((eq thingy/current-theme thingy/dark-theme)
         (thingy/change-theme thingy/light-theme))
        ((eq thingy/current-theme thingy/light-theme)
         (thingy/change-theme thingy/dark-theme))))

(defun thingy/set-font-faces ()
  "Sets the fonts"
  (interactive)
  (message "lol")
  ;; (set-face-attribute 'default nil :font "JuliaMono" :height thingy/default-font-size)
  (set-face-attribute 'fixed-pitch nil :font "Unifont" :height thingy/default-font-size)
  (set-face-attribute 'variable-pitch nil :font "Unifont" :height thingy/default-font-size))


(thingy/set-font-faces)
(defun thingy/daemon-start ()
  "Start daemon (on Windows) and set font faces"
  (thingy/set-font-faces))

;; --- Daemon stuff ---
; (if (daemonp)
;     (add-hook 'server-after-make-frame-hook
;               'thingy/daemon-start)
;     (message "hello there")
;     (server-start)
;     (thingy/set-font-faces))

;; --- Start Page ---
;; (define-minor-mode start-mode
;;   "Minor mode for the start page"
;;   :lighter " start"
;;   :keymap (let ((map (make-sparse-keymap)))
;;             ;; --- key bindings ---
;;             ;; (define-key map (kbd "1") '(find-file "~/.emacs.d/init.el"))
;;             (general-define-key
;;              :keymaps 'start-mode-map
;;              "1" '((find-file "~/.emacs.d/init.el")))
;;             map))
;; (define-minor-mode start-mode
;;   "Minor mode for the start page"
;;   :lighter " start"
;;   :keymap (let ((map (make-sparse-keymap)))
;;             (evil-define-key 'normal 'start-mode-map
;;               (kbd "1") 'ibuffer)
;;             map))

;; (provide 'start-mode)
;; (add-hook 'start-mode-hook 'read-only-mode)
;; (add-hook 'after-init-hook #'start-mode)
