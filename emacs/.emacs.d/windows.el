(custom-set-variables
 '(auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaves\\1" t)))
 '(backup-directory-alist '((".*" . "~/.emacs.d/backups")))
 '(undo-tree-history-directory-alist '((".*" . "~/.emacs.d/undo-tree"))))

(setq default-directory "c:/Users/gavin/")

;; --- Start page ---
(defvar thingy/start-page-file "~/.emacs.d/start-page.org")
(setq initial-buffer-choice 'thingy/start-page-file)
